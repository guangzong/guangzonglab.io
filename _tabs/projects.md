---
title: pojects
icon: fas fa-project-diagram
order: 5
---
##### RPN on MNIST
* 02 2021 - 04 2021
* generate image contain multiple MNIST figure.
* using RPN detect the handwriting digital position
* small implementation of RPN. It did not implement multiple feature proposed in the paper.
* [github link](https://github.com/chen-gz/fake_faster_rcnn)

##### eButton hardware and software
* 2020 - 2021 (in progress)
* aim to collect image over 10 hours without charge
* it's a wearable device, so it will be very small
* base on ESP32 (may change to other chip in the future)

##### Food/Non-Food Classification of Real-Life Egocentric Images.   
* 2020 - 2021 (in progress)
* publication: 
* [github link](https://github.com/chen-gz/food_detection)


#####  FOC motor control (Nimbus Robotics)
* 2020
* Base on TI and ST chip, use FOC to control BLDC motor.

##### CNN on MCU
* 2020
* implement CNN on STM32 MCU
* [gihtub link]()

##### human face base on ARM and elm
* 2020 with dada(main contributer)
* implement face recognition algorithm in MCU base Opencv and elm algorithm
* [github link](https://github.com/1997huoda/face-recognition-system-ARM)

##### Omnidirectional vision measurement and map construction of mobile robot
* 2018 - 2019 
* mainly focus on distance measurement base on laser.
* Document(BIT thesis) Omni-directional vision measurement and map construction of mobile robot
* This project have code but not share in github.

#####  Environment detect vehicle based on STM32
* before 2019 (2016 - 2017)
* The project aims at design and builds a vehicle based on STM32 to detect the environment without human control and feedback signals.  This project can reduce the risk of exploring unknown environments and avoid accidents. 



