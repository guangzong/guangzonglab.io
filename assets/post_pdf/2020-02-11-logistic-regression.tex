\documentclass[a4paper,english,14pt]{extarticle}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage[english]{babel}
\usepackage{hyperref}
\usepackage{amsfonts}
\makeatletter

\special{papersize=\the\paperwidth,\the\paperheight}
\theoremstyle{definition}
\newtheorem*{defn*}{\protect\definitionname}

\def\th@plain{%
  \thm@notefont{}% same as heading font
  \itshape % body font
}
\def\th@definition{%
  \thm@notefont{}% same as heading font
  \normalfont % body font
}

\renewcommand{\qedsymbol}{$\blacksquare$}

\makeatother

\usepackage{babel}
\providecommand{\definitionname}{Definition}

\begin{document}
% \maketitle
\subsection{Logistic regression}
The function $\frac{1}{1+e^{-t}}$ is called logistic function (or a sigmoid function). \href{https://raw.githubusercontent.com/chen-gz/picBed/master/20210211152238.png}{picture}

\begin{enumerate}
\item Assume $\eta(x)=\frac{1}{1+e^{-\left(w^{T} x+b\right)}}\left(w \in \mathbb{R}^{d}, b \in \mathbb{R}\right)$ \\
(This is all we need to know to implement Bayes classifier and we assume it is of this form. This is true for the case where our classes are both multivariate Gaussian densities with same covariance matrix. But it is actually true for a lot of different kind of densities. In other words, you can show this arises in other cases besides this specific LDA model).
\item Estimate $w, b$ somehow from the data
\item Plug the estimate
$$
\hat{\eta}(x)=\frac{1}{1+e^{-\left(\hat{w}^{T} x+\hat{b}\right)}}
$$
into the formula for Bayes classifier. Basically pretending that this is the true a posteriori.
\end{enumerate}

What happens if we actually do this? Let's denote the logistic regression classifier by
$$
\hat{f}(x)=1_{\{\hat{\eta}(x) \geq 1 / 2\}}(x)
$$
Again, this is a indicator function notation.

Note that
$$
\begin{aligned}
\hat{f}(x)=1 & \Longleftrightarrow \hat{\eta}(x) \geq 1 / 2\\
& \Longleftrightarrow \frac{1}{1+e^{-\left(\hat{w}^{T} x+\hat{b}\right)}} \geq \frac{1}{2}  \\
& \Longleftrightarrow e^{-\left(\left(\hat{w}^{T} x+\hat{b}\right)\right.} \leq 1  \\
& \Longleftrightarrow\left(\hat{w}^{T} x+\hat{b}\right) \geq 0
\end{aligned}
$$
So, once again we got,
$$
\hat{f}(x)=\left\{\begin{array}{ll}
1 & \text { if } \hat{w}^{T} x+\hat{b} \geq 0 \\
0 & \text { otherwise }
\end{array}\right.
$$

To do logistic regression, obviously we have to estimate the parameters for $w, b$.
$$
\hat{\eta}(x)=\frac{1}{1+e^{-\left(w^{T} x+b\right)}}
$$

In order to talk about this estimation, we have to take a \textbf{detour} to \textbf{maximum likelihood estimation (MLE)}.

For convenience, let's let $\theta=(b, w)$
Note that $\eta(x)$ is really function of both $x$ and $\theta,$ so we will use the notation $\eta(x ; \theta)$ to highlight this dependence.

Suppose we knew $\theta$, then we could compute \\
$\mathbb{P}\left[y_{i} \mid x_{i} ; \theta\right]=\mathbb{P}\left[Y_{i}=y_{i} \mid X_{i}=x_{i} ; \theta\right]$ this is just a shorthand
$$
\begin{array}{l}
=\left\{\begin{array}{ll}
\eta\left(x_{i} ; \theta\right) & \text { if } y_{i}=1 \\
1-\eta\left(x_{i} ; \theta\right) & \text { if } y_{i}=0 \\
= & \eta\left(x_{i} ; \theta\right)^{y_{i}}\left(1-\eta\left(x_{i} ; \theta\right)\right)^{1-y_{i}}
\end{array}\right.
\end{array}
$$
This is convenient way to express probability- because of conditional independence, we also have that
$$
\begin{aligned}
\mathbb{P}\left[y_{1}, \ldots, y_{n} \mid x_{1}, \ldots, x_{n} ; \theta\right] &=\prod_{i=1}^{n} \mathbb{P}\left[y_{i} \mid x_{i} ; \theta\right] \\
&=\prod_{i=1}^{n} \eta\left(x_{i} ; \theta\right)^{y_{i}}\left(1-\eta\left(x_{i} ; \theta\right)\right)^{1-y_{i}}
\end{aligned}
$$

Suppose we view $y_{1}, \ldots, y_{n}$ to be fixed, and view $\mathbb{P}\left[y_{1}, \ldots, y_{n} \mid x_{1}, \ldots, x_{n} ; \theta\right]$ as just the function of $\theta$. In other words, if we change the parameters we are going to get a different a posteriori probability. We actually observed $y_{1}, \ldots, y_{n},$ so you can view this function $\mathbb{P}\left[y_{1}, \ldots, y_{n} \mid x_{1}, \ldots, x_{n} ; \theta\right]$ as telling us something about how much sense do those parameters make. So if you give me parameters $\theta$ and I plug them in and this turns out to be $0,$ then I know that those parameters makes no sense. On the other hand, if you have given me a $\theta$ and I plugin to this and it gets to be equal to be $1,$ yay. If those parameters are true, then this would make a lot of sense. Because if we have these parameters, given these inputs $x_{1}, \ldots, x_{n}$ we would expect to see this outputs $y_{1}, \ldots, y_{n}$ and we did see this outputs. This is the idea in maximum likelihood estimation!

When we do this, $L(\theta)=\mathbb{P}\left[y_{1}, \ldots, y_{n} \mid x_{1}, \ldots, x_{n} ; \theta\right]$ is called the likelihood function.

We've got to observe some inputs and outputs, and there is some unknown parameter $\theta$ that we like to estimate. What we can do is, we can calculate the probability of these outputs given our inputs, and we are going to pick $\theta$ to maximize this likelihood. So, the method of maximum likelihood aims to estimate $\theta$ by finding the $\theta$ that maximizes the likelihood $L(\theta)$. This is not particularly surprising.

Not that the likelihood function here is not a PDF. It is not actually computing any kind of probability, you view this as a function of $\theta$ because theta is not really a random quantity. is just some deterministic parameter we are trying to estimate. And in fact, if you very for all the different possible $\theta$ values, in general $\mathbb{P}\left[y_{1}, \ldots, y_{n} \mid x_{1}, \ldots, x_{n} ; \theta\right]$ will not integrate to $1 .$ When you think about MLE, you shouldn't be thinking that we are calculating the probability of $\theta$, we are thinking of "likelihood". This may confuse some of you since we have been talking about lots of probabilities (a priori, a posteriori, etc.), and now we are talking about the likelihood. And in English "probability" and "likelihood" pretty much mean the same thing, but in this context they are referring to very specific kind of functions. Probability is associated with random variable, in here we don't necessary think of $\theta$ being random. Likelihood of some deterministic variable conditioned upon random events occurring.

In practice, it i often more convenient to maximize the log-likelihood, i.e., log $L(\theta) .$ Remember below is the likelihood function for our particular problem.

$$
L(\theta)=\prod_{i=1}^{n} \eta\left(x_{i} ; \theta\right)^{y_{i}}\left(1-\eta\left(x_{i} ; \theta\right)\right)^{1-y_{i}}
$$
Basically whenever you are talking about he likelihood function and you have bunch of independent random variables involved, you end up with these products and it is just a pain to deal with them most of the time. But if take log of the likelihood, then the products becomes a sum and essentially it is easier to work with. Since the log is a monotonic transformation, maximizing $l(\theta)$ is equivalent to maximizing $L(\theta)$.
$$
\begin{aligned}
l(\theta)=& \log L(\theta) \\
&=\sum_{i=1}^{n} y_{i} \log \eta\left(x_{i} ; \theta\right)+\left(1-y_{i}\right) \log \left(1-\eta\left(x_{i} ; \theta\right)\right)
\end{aligned}
$$

In Logistic regression, we are assuming $\eta(x ; \theta)$ has a particular form, and that form depends on these parameters $\theta$. So we are going to choose $\theta$ to maximize the log-likehood. This is actually all there is to it. Let's simplify the log likelihood a bit so we can implement easier.

Notation:
$$
\begin{array}{l}
\tilde{\mathrm{x}}=[1, x(1), \ldots, x(d)]^{T} \\
\boldsymbol{\theta}=[b, w(1), \ldots, w(d)]^{T}
\end{array}
$$

This means that $w^{T} x+b=\boldsymbol{\theta}^{T} \tilde{\mathrm{x}},$ which lets us write
$$
\eta\left(x_{i} ; \boldsymbol{\theta}\right)=\frac{1}{1+e^{-\boldsymbol{\theta} \tilde{\mathbf{x}}}}
$$
Thus, if we let $g(t)=\frac{1}{1+e^{-t}},$ the we can write
$$
l(\theta)=\sum_{i=1}^{n} y_{i} \log g\left(\boldsymbol{\theta}^{T} \tilde{\mathrm{x}}\right)+\left(1-y_{i}\right) \log \left(1-g\left(\boldsymbol{\theta}^{T} \tilde{\mathrm{x}}\right)\right)
$$
What is the $\log$ and $1-\log$ of $g(t) ?$


\textbf{Facts:}
$$
\begin{aligned}
\log g(t) &=\log \left(\frac{1}{1+e^{-t}}\right)=-\log \left(1+e^{-t}\right) \\
\log (1-g(t)) &=\log \left(1-\frac{1}{1+e^{-t}}\right) \\
&=\log \left(\frac{e^{-t}}{1+e^{-t}}\right)=-t-\log \left(1+e^{-t}\right) \\
&=\log \left(\frac{1}{1+e^{t}}\right)=-\log \left(1+e^{t}\right)
\end{aligned}
$$

Thus,
$$
\begin{aligned}
l(\theta) &=\sum_{i=1}^{n} y_{i} \log g\left(\boldsymbol{\theta}^{T} \tilde{\mathrm{x}}_{i}\right)+\left(1-y_{i}\right) \log \left(1-g\left(\boldsymbol{\theta}^{T} \tilde{\mathrm{x}}\right)\right) \\
&=\sum_{i=1}^{n}-y_{i} \log \left(1+e^{-\boldsymbol{\theta}^{T} \tilde{\mathrm{x}}_{i}}\right)-\log \left(1+e^{\boldsymbol{\theta}^{T} \tilde{\mathrm{x}}_{i}}\right)-y_{i}\left(-\boldsymbol{\theta}^{T} \tilde{\mathrm{x}}_{i}-\log \left(1+e^{-\boldsymbol{\theta}^{T} \tilde{\mathrm{x}}_{i}}\right)\right) \\
&=\sum_{i=1}^{n} y_{i} \boldsymbol{\theta}^{T} \tilde{\mathrm{x}}_{i}-\log \left(1+e^{\boldsymbol{\theta}^{T} \tilde{\mathrm{x}_{i}}}\right)
\end{aligned}
$$

How can we maximize
$$
l(\theta)=\sum_{i=1}^{n} y_{i} \boldsymbol{\theta}^{T} \tilde{\mathrm{x}}_{i}-\log \left(1+e^{\boldsymbol{\theta}^{T} \tilde{x}_{i}}\right)
$$

with respect to $\boldsymbol{\theta} ?$ I could take the derivative and set it to $0,$ only here we have a bunch of things we want to solve for at once, so we need the partial derivatives.

Find a $\boldsymbol{\theta}$ such that $\nabla l(\boldsymbol{\theta})=\left[\begin{array}{c}\frac{\partial l(\boldsymbol{\theta})}{\partial \boldsymbol{\theta}_{1}} \\ \cdot \\ \cdot \\ \frac{\partial l(\boldsymbol{\theta})}{\partial \boldsymbol{\theta}_{d+1}}\end{array}\right]=0$ It is not hard to show that

$$
\begin{aligned}
 \nabla l(\theta) &=\sum_{i=1}^{n} \nabla\left(y_{i} \boldsymbol{\theta}^{T} \tilde{\mathbf{x}}_{i}-\log \left(1+e^{\boldsymbol{\theta}^{T} \tilde{\mathbf{x}}_{i}}\right)\right) \\ &=\sum_{i=1}^{n} y_{i} \tilde{x}_{i}-\tilde{\mathbf{x}}_{i} e^{\boldsymbol{\theta}^{T} \tilde{\mathbf{x}}_{i}}\left(1+e^{\boldsymbol{\theta}^{T} \tilde{x}_{i}}\right)^{-1} \\ &=\sum_{i=1}^{n} \tilde{\mathbf{x}}_{i}\left(y_{i}-g\left(\boldsymbol{\theta}^{T} \tilde{\mathbf{x}}_{i}\right)\right) 
\end{aligned}
$$

Remember the dimension of $\tilde{\mathrm{x}}_{i}$ is $d+1$. this gave us $d+1$ equations, but they are nonlinear and have no-closed form solutions. We need to somehow solve an optimization problem.

Optimization
$$
\min _{x \in \mathbb{R}^{d}} f(x)
$$
(or $\min _{\boldsymbol{\theta} \in \mathbb{R}^{d+1}}-\log (\boldsymbol{\theta})$ for today)
In most cases, we cannot compute the solution simply by setting $\nabla f(x)=0$ and solving for $x$ on paper-who would wanna do that anyway. However, there are many powerful algorithms for finding $x$. And the simplest possible algorithm for this is, yes you guessed it, Gradient Descent.

A simple way to try to find the minimum our objective function is to iteratively roll downhill.
From initial guess $x^{0}$, take a step in the direction of the negative gradient.

Notice we know how to calculate gradient for our problem, what we didn't know is how to solve when we set it to zero.
$$
\begin{array}{l}
x^{1}=x^{0}-\left.\alpha_{0} \nabla f(x)\right|_{x=x^{0}}, \quad \alpha_{0}: \text { "step size" } \\
x^{2}=x^{1}-\left.\alpha_{1} \nabla f(x)\right|_{x=x^{1}} \\
\vdots
\end{array}
$$

Convergence of gradient descent The core iteration of gradient descent is to compute
$$
x^{j+1}=x^{j}-\left.\alpha_{j} \nabla f(x)\right|_{x=x^{j}}
$$

Note that if $\left.\nabla f(x)\right|_{x=x^{j}}=0,$ then we have found the minimum and $x^{j+1}=x^{j},$ so the algorithm will terminate.

It turns out that if $f$ is convex, then the gradient descent (with a fixed step size $\alpha$ ) is guaranteed to converge to the global minimum of $f$.
\href{https://raw.githubusercontent.com/chen-gz/picBed/master/20210211172718.png}{picture}


\subsection{Newton-raphson method}
Also known as the Newton-Raphson method, this approach can be viewed as simply using the second derivative (Hessian matrix) to automatically select an appropriate step size.
$$
x^{j+1}=x^{j}-\left.\left(\nabla^{2} f(x)\right)^{-1} \nabla f(x)\right|_{x=x^{j}}
$$
Newton's method, if you can do it, tends to be very powerful-if we can invert the Hessian matrix $\left(\nabla^{2} f(x)\right)^{-1}$. In most cases this could not be feasible however for logistic regression it does work. We can compute this.

The negative log-likelihood in logistic regression is a convex function. That means gradient descent guaranteed to converge, if we have the right step size. But also Newton's method can work. So both gradient descent and Newton's method are common strategies for setting the parameters in logistic regression.

Newton's method, typically, is much faster when the dimension $d$ is small, but is impractical when $d$ is large, order $d^{3}$ computation and there is also memory concerns. Can you even form the Hessian matrix? ( -maybe more on this next week's homework)


\end{document}
